# -*- mode: ruby -*-
# vi: set ft=ruby :
##### .bashrc/.bash_profile 
GIT_REPO = ENV['GIT_REPO'] || 'gitlab.com'
PYPI_HOST = ENV['PYPI_HOST'] || ''
CA = ENV['CA'] || ''
KEY = ENV['KEY'] || ''
ORG = ENV['ORG'] || ''
RHSM_USER = ENV['RHSM_USER'] || ''
RHSM_PASS = ENV['RHSM_PASS'] || ''
#####

##### package updates and installs
$setup = <<~SETUP
  sed -i "/installonly_limit=*/a http_caching=none\n" /etc/yum.conf ## does away with old cached data, which my cause yum error
  yum clean all
  rm -rf /var/cache/yum/
  yum update -y
  yum install -y \
    libXmu \
    libXt \
    kernel-devel \
    xorg-x11-drivers \
    xorg-x11-utils \
    vim \
    unzip \
    wget \
    vim-enhanced ansible \
    ansible-lint \
    git \
    gcc \
    libffi-devel \
    libselinux-python \
    openssl-devel \
    python36 \
    python-devel
SETUP
#####

##### enable epel repository
$epel = <<~EPEL
  curl -sk https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm -o /tmp/epel-release-latest-7.noarch.rpm
  rpm -Uvh /tmp/epel-release-latest-7.noarch.rpm && rm -rf /tmp/epel-release-latest-7.noarch.rpm
  sed -i 's/^#baseurl/baseurl/g' /etc/yum.repos.d/epel.repo
  sed -i 's/^metalink/#metalink/g' /etc/yum.repos.d/epel.repo
EPEL
#####

##### Setup SSH keys for ansible-galaxy
$ssh = <<~SSH
  # Fix the file permissions because evidently stuff -> https://github.com/hashicorp/vagrant/issues/5561
  chmod 0644 /home/vagrant/.ssh/known_hosts
  chmod 0600 /home/vagrant/.ssh/authorized_keys
  chmod 0600 /home/vagrant/.ssh/id_rsa
  chmod 0644 /home/vagrant/.ssh/id_rsa.pub
  chmod 0600 /home/vagrant/.gitconfig

  # Disable password auth
  sed -i -e '\\#PasswordAuthentication yes# s#PasswordAuthentication yes#PasswordAuthentication no#g' /etc/ssh/sshd_config
  systemctl restart sshd
SSH

##### Virtual box build parameters

Vagrant.configure("2") do |config|
  config.vm.box = "mrlen/rhel-7-efi"
  config.vm.boot_timeout = 2400
  config.vm.provider 'virtualbox' do |v|
    v.memory = 4096
    v.cpus = 2
    v.customize ["modifyvm", :id, "--vram", "128"]
    v.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
    v.customize ["modifyvm", :id, "--accelerate3d", "on"]
    v.customize ["modifyvm", :id, "--firmware", "efi"]

  end
#####

##### set hostname for VM

  if Vagrant::Util::Platform.windows? then
    config.vm.hostname = "vagrant-#{ENV['COMPUTERNAME']}#{ENV['USERDNSDOMAIN']}".downcase
  elsif  Vagrant::Util::Platform.linux? then
    config.vm.hostname = "vagrant-#{`hostname -f`}"
  end
##### 

##### register to Red Hat subscription

  if CA != '' && KEY != '' &&  ORG != ''
    config.vm.provision 'subscription', type: 'shell', inline: <<~SUBSCRIBE
      rpm -Uvh #{CA}
      subscription-manager register --activationkey=#{KEY} --org=#{ORG} --force
    SUBSCRIBE
  elsif RHSM_USER != '' && RHSM_PASS != ''
    config.vm.provision 'subscription', type: 'shell', inline: <<~SUBSCRIBE
      subscription-manager clean
      subscription-manager register --username=#{RHSM_USER} --password=#{RHSM_PASS} --auto-attach
      subscription-manager repos --enable=rhel-7-server-extras-rpms --enable=rhel-7-server-optional-rpms
    SUBSCRIBE
  end
#####

##### Use local ssh keys instead of Vagrant insecure
  # config.ssh.forward_agent = true
  config.ssh.insert_key = false
  config.ssh.private_key_path = ['~/.ssh/id_rsa', '~/.vagrant.d/insecure_private_key']
  config.vm.provision 'file', source: '~/.ssh/known_hosts', destination: '~/.ssh/known_hosts'
  config.vm.provision 'file', source: '~/.ssh/id_rsa.pub', destination: '~/.ssh/authorized_keys'
  config.vm.provision 'file', source: '~/.ssh/id_rsa', destination: '~/.ssh/id_rsa'
  config.vm.provision 'file', source: '~/.ssh/id_rsa.pub', destination: '~/.ssh/id_rsa.pub'
end
#####
